{% from slspath+"/map.jinja" import mongodb_exporter with context %}

mongodb_exporter-create-user:
  user.present:
    - name: {{ mongodb_exporter.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

mongodb_exporter-create-group:
  group.present:
    - name: {{ mongodb_exporter.service_group }}
    - system: True
    - members:
      - {{ mongodb_exporter.service_user }}
    - require:
      - user: {{ mongodb_exporter.service_user }}

mongodb_exporter-bin-dir:
  file.directory:
   - name: {{ mongodb_exporter.bin_dir }}
   - makedirs: True

mongodb_exporter-dist-dir:
 file.directory:
   - name: {{ mongodb_exporter.dist_dir }}
   - makedirs: True

mongodb_exporter-install-binary:
  file.managed:
    - name:  {{ mongodb_exporter.dist_dir }}/mongodb_exporter-{{ mongodb_exporter.version }}
    - source: salt://{{ slspath }}/files/mongodb_exporter-{{ mongodb_exporter.version }}
    - user: {{ mongodb_exporter.service_user }}
    - group: {{ mongodb_exporter.service_group }}
    - mode: 0755


mongodb_exporter-symlink-binary:
  file.symlink:
    - name: {{ mongodb_exporter.bin_dir }}/mongodb_exporter
    - target: {{ mongodb_exporter.dist_dir }}/mongodb_exporter-{{ mongodb_exporter.version }}
    - mode: 0755
