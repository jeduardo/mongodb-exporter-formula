describe user('mongodb-exporter') do
  it { should exist }
end

describe group('mongodb-exporter') do
  it { should exist }
end

describe file('/usr/bin/mongodb_exporter') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
end

describe file('/usr/lib/mongodb_exporter/dist') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/systemd/system/mongodb_exporter.service.d') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/systemd/system/mongodb_exporter.service.d/env.conf') do
 it { should exist }
 it { should be_owned_by 'root' }
end

describe service('mongodb_exporter') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
